/*
MoniBabyTor - a webcam based monitoring app for babies
    Copyright (C) 2013 Assaf Paz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MJPEGHANDLER_H
#define MJPEGHANDLER_H

#include <QObject>
#include <QIODevice>
#include <QImage>

class MJpegFile : public QObject
{
    Q_OBJECT
public:
    explicit MJpegFile(QObject *parent = 0);
    
    void setVideoDevice(QIODevice* videoDevice);

signals:
    void imageReceived(QImage* image);
    
public slots:
    void imageDataReady();
    
private:
    QIODevice* m_videoDevice;
};

#endif // MJPEGHANDLER_H
