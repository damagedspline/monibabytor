#-------------------------------------------------
#
# Project created by QtCreator 2013-03-02T19:54:32
#
#-------------------------------------------------

folder_01.source = qml/MoniBabyTor
folder_01.target = qml
DEPLOYMENTFOLDERS = folder_01

QT       += core gui

TARGET = MoniBabyTor
TEMPLATE = app

include(qmlapplicationviewer/qmlapplicationviewer.pri)

SOURCES += main.cpp\
        mainwindow.cpp \
        cameralogic.cpp \
        wavfile.cpp \
        utils.cpp \
    settingsdatamodel.cpp \
    mjpegfile.cpp \
    VideoWidgetSurface.cpp \
    videowidget.cpp \
    rmslevel.cpp \
    videoview.cpp

HEADERS  += mainwindow.h \
        cameralogic.h \
        wavfile.h \
        utils.h \
    settingsdatamodel.h \
    mjpegfile.h \
    videowidgetsurface.h \
    videowidget.h \
    rmslevel.h \
    videoview.h

FORMS    += mainwindow.ui

CONFIG += mobility
MOBILITY = multimedia

symbian {
    TARGET.UID3 = 0xe1995d13
    # TARGET.CAPABILITY += 
    TARGET.EPOCSTACKSIZE = 0x14000
    TARGET.EPOCHEAPSIZE = 0x020000 0x800000
}

OTHER_FILES += \
    qtc_packaging/debian_harmattan/rules \
    qtc_packaging/debian_harmattan/README \
    qtc_packaging/debian_harmattan/manifest.aegis \
    qtc_packaging/debian_harmattan/copyright \
    qtc_packaging/debian_harmattan/control \
    qtc_packaging/debian_harmattan/compat \
    qtc_packaging/debian_harmattan/changelog \
    qtc_packaging/debian_fremantle/rules \
    qtc_packaging/debian_fremantle/README \
    qtc_packaging/debian_fremantle/copyright \
    qtc_packaging/debian_fremantle/control \
    qtc_packaging/debian_fremantle/compat \
    qtc_packaging/debian_fremantle/changelog \
    qtc_packaging/debian_ubuntu_quantal/rules \
    qtc_packaging/debian_ubuntu_quantal/README \
    qtc_packaging/debian_ubuntu_quantal/copyright \
    qtc_packaging/debian_ubuntu_quantal/control \
    qtc_packaging/debian_ubuntu_quantal/compat \
    qtc_packaging/debian_ubuntu_quantal/changelog \
    qtc_packaging/debian_ubuntu_raring/rules \
    qtc_packaging/debian_ubuntu_raring/README \
    qtc_packaging/debian_ubuntu_raring/copyright \
    qtc_packaging/debian_ubuntu_raring/control \
    qtc_packaging/debian_ubuntu_raring/compat \
    qtc_packaging/debian_ubuntu_raring/changelog

# Please do not modify the following two lines. Required for deployment.
include(deployment.pri)
qtcAddDeployment()

RESOURCES += \
    resources.qrc
