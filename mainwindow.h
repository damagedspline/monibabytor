/*
MoniBabyTor - a webcam based monitoring app for babies
    Copyright (C) 2013 Assaf Paz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVideoFrame>
#include "settingsdatamodel.h"
#include "videowidgetsurface.h"
#include "videowidget.h"
#include "mjpegfile.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(SettingsDataModel& dataModel, MJpegFile& mjpegFile, QWidget *parent = 0);
    ~MainWindow();

private slots:
    void addImage(QImage* image);
    void onVideoTimeout();
    void on_actionButton_clicked();
    void showSettings();
    void addPresetButtonClicked();
    void removePresetButtonClicked();
    void loadPresetButtonClicked();
    void loadSettings();
    void refreshOnPresetChanged();

signals:
    void startRequested();
    void stopRequested();

private:
    Ui::MainWindow *ui;
    MJpegFile& _mjpegFile;

    SettingsDataModel& _dataModel;
    VideoWidget* _videoWidget;
    bool _videoFormatInitialized;
    bool _isStarted;
};

#endif // MAINWINDOW_H
