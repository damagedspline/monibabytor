/*
MoniBabyTor - a webcam based monitoring app for babies
    Copyright (C) 2013 Assaf Paz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CAMERALOGIC_H
#define CAMERALOGIC_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QTimer>
#include "wavfile.h"
#include "settingsdatamodel.h"
#include "mjpegfile.h"

class CameraLogic : public QObject
{
    Q_OBJECT
public:
    explicit CameraLogic(SettingsDataModel& dataModel, MJpegFile& mjpegFile, WavFile& waveFile, QObject *parent = 0);
    virtual ~CameraLogic();
    
signals:
    void imageReceived(QImage* image);
    void error(QString errorMsg);
    
private slots:
    void streamEnded(QNetworkReply* reply);
    void videoFeedTimeout();

public slots:
    void start();
    void stop();
    void onPeakDetected();

private:
    QNetworkAccessManager _nam;
    QNetworkReply* _audioFeed;
    QNetworkReply* _videoFeed;
    WavFile& _wavFile;
    MJpegFile& _mjpegFile;
    SettingsDataModel& _dataModel;
    QTimer _videoTimeoutTimer;
};

#endif // CAMERALOGIC_H
