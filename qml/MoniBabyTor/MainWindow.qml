import QtQuick 1.1
import com.nokia.meego 1.1

PageStackWindow {
    id: rootWindow

    property string error: ""

    Connections {
        target: dataModel
        onError: {
            rootWindow.error = errorMessage;
        }
        onVideoTimeoutRequested: {
            videoPage.timeout();
        }
    }

    SettingsPage {
        id: settingsPage

        property bool loaded: false

        error: rootWindow.error

        onStartRequested: {
            dataModel.startRequested();
        }

        Component.onCompleted: {
            videoUrl = dataModel.videoUrl;
            audioUrl = dataModel.audioUrl;
            userName = dataModel.username;
            password = dataModel.password;
            videoTimeout = dataModel.videoTimeout;
            audioSensitivity = dataModel.sensitivity;
            outputVolume = dataModel.outputVolume;
            loaded = true;
        }

        onVideoUrlChanged: {
            if (loaded) dataModel.videoUrl = videoUrl;
        }

        onAudioUrlChanged: {
            if (loaded) dataModel.audioUrl = audioUrl;
        }

        onUserNameChanged: {
            if (loaded) dataModel.username = userName;
        }

        onPasswordChanged: {
            if (loaded) dataModel.password = password;
        }

        onVideoTimeoutChanged: {
            if (loaded) dataModel.videoTimeout = videoTimeout;
        }

        onAudioSensitivityChanged: {
            if (loaded) dataModel.sensitivity = audioSensitivity;
        }

        onOutputVolumeChanged: {
            if (loaded) dataModel.outputVolume = outputVolume;
        }
    }

    VideoPage {
        id: videoPage

        threshold: settingsPage.audioSensitivity

        error: rootWindow.error

        videoViewFeed: videoFeed

        onStopRequested: {
            dataModel.stopRequested();
        }

        Connections {
            target: dataModel
            onRmsLevelChanged: {
                videoPage.audioLevel = level;
            }
        }
    }

    initialPage: settingsPage
}
