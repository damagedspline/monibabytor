import QtQuick 1.1
import com.nokia.meego 1.1
import monibabytor 1.0

Page {
    id: videoPage

    property alias threshold: audioLevel.threshold
    property alias audioLevel: audioLevel.value
    property alias error: errorLabel.text

    property alias videoViewFeed: videoView.videoFeed

    signal stopRequested()

    function timeout()
    {
        videoView.onVideoTimeout();
    }

    VisualItemModel {
        id: itemModel

        VideoView {
            id: videoView
            width: 320
            height: 240
        }

        Rectangle {
            id: audioLevel

            property int minValue: 0
            property int maxValue: 1000
            property int value: 500
            property int threshold: 700

            border.color: "black"
            border.width: 5

            radius: 5

            width: videoFeed.width

            height: 30
            color: "white"

            Rectangle {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom

                border.color: "black"
                border.width: 2

                radius: 5

                width: parent.value*parent.width/(parent.maxValue - parent.minValue)
                color: "#4f63f9"
            }

            Rectangle {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                width: 5
                color: "red"
                x: parent.threshold*parent.width/(parent.maxValue - parent.minValue) - width/2
            }
        }
    }

    ScrollDecorator {
        flickableItem: videoFeed
    }

    ListView {
        id: videoFeed
        anchors.fill: parent
        anchors.margins: 30
        spacing: 20
        model: itemModel
    }

    tools: ToolBarLayout {
        visible: false

        ToolIcon { iconId: "toolbar-mediacontrol-stop"; onClicked: { stopRequested(); pageStack.pop(); } }

        Label {
            id: errorLabel

            visible: text !== ""
        }
    }

}
