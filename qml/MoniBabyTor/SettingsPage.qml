import QtQuick 1.1
import com.nokia.meego 1.1

Page {
    id: settingsPage

    property alias videoUrl: videoUrlText.text;
    property alias audioUrl: audioUrlText.text;
    property alias userName: userNameText.text;
    property alias password: passwordText.text;
    property alias videoTimeout: videoTimeoutText.text;
    property alias audioSensitivity: audioSensitivityInt.value;
    property alias outputVolume: outputVolumeInt.value;

    property alias error: errorLabel.text

    signal startRequested()

    ScrollDecorator {
        flickableItem: formGrid
    }

    property int buttonWidth: parent.width / 2
    property int labelWidth: parent.width / 3

    VisualItemModel {
        id: itemModel

        Row {
            Label {
                text: "Video URL"
                width: labelWidth
            }

            TextField {
                id: videoUrlText
                width: buttonWidth
            }
        }

        Row {
            Label {
                text: "Audio URL"
                width: labelWidth
            }

            TextField {
                id: audioUrlText
                width: buttonWidth
            }
        }

        Row {
            Label {
                text: "User Name"
                width: labelWidth
            }

            TextField {
                id: userNameText
                width: buttonWidth
            }
        }

        Row {
            Label {
                text: "Password"
                width: labelWidth
            }

            TextField {
                id: passwordText
                width: buttonWidth

                echoMode: TextInput.PasswordEchoOnEdit
            }
        }

        Row {
            Label {
                text: "Video Timeout"
                width: labelWidth
            }

            Button {
                id: videoTimeoutText
                width: buttonWidth

                onClicked: {
                    timeoutDialog.open();
                }
            }
        }

        Row {
            Label {
                text: "Audio Sensitivity"
                width: labelWidth
            }

            Slider {
                id: audioSensitivityInt

                minimumValue: 0
                maximumValue: 1000

                stepSize: 50
                width: buttonWidth
            }
        }

        Row {
            Label {
                text: "Output Volume"
                width: labelWidth
            }

            Slider {
                id: outputVolumeInt

                minimumValue: 0
                maximumValue: 100

                stepSize: 10
                width: buttonWidth
            }
        }
    }

    ListView {
        id: formGrid
        anchors.fill: parent
        anchors.margins: 30

        model: itemModel
    }

    tools: ToolBarLayout {
        visible: false

        ToolIcon { iconId: "toolbar-mediacontrol-play"; onClicked: { pageStack.push(videoPage); startRequested(); } }

        Label {
            id: errorLabel

            visible: text !== ""
        }
    }

    SelectionDialog {
        id: timeoutDialog
        titleText: "Video timeout:"
        model: ListModel {
            ListElement { value: "15" }
            ListElement { value: "30" }
            ListElement { value: "60" }
            ListElement { value: "120" }
        }

        onSelectedIndexChanged: {
            videoTimeoutText.text = model.get(selectedIndex).value;
            timeoutDialog.close();
        }
    }

}
