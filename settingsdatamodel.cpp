/*
MoniBabyTor - a webcam based monitoring app for babies
    Copyright (C) 2013 Assaf Paz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "settingsdatamodel.h"

SettingsDataModel::SettingsDataModel(QObject *parent) :
    QObject(parent)
{
    _settings.beginGroup("UserData");
    _preset = _settings.value("Preset", "DLink DCS-932L").toString();
    _settings.endGroup();

    loadPreset(_preset);
}

QString SettingsDataModel::preset() const
{
    return _preset;
}

QString SettingsDataModel::videoUrl() const
{
    return _videoUrl;
}

void SettingsDataModel::setVideoUrl(QString videoUrl)
{
    if (_videoUrl == videoUrl) return;

    _videoUrl = videoUrl;
    emit videoUrlChanged(_videoUrl);
}

QString SettingsDataModel::audioUrl() const
{
    return _audioUrl;
}

void SettingsDataModel::setAudioUrl(QString audioUrl)
{
    if (_audioUrl == audioUrl) return;

    _audioUrl = audioUrl;
    emit audioUrlChanged(_audioUrl);
}

QString SettingsDataModel::username() const
{
    return _username;
}

void SettingsDataModel::setUsername(QString username)
{
    if (_username == username) return;

    _username = username;
    emit usernameChanged(_username);
}

QString SettingsDataModel::password() const
{
    return  _password;
}

void SettingsDataModel::setPassword(QString password)
{
    if (_password == password) return;

    _password = password;
    emit passwordChanged(_password);
}

int SettingsDataModel::sensitivity() const
{
    return _sensitivity;
}

void SettingsDataModel::setSensitivity(int sensitivity)
{
    if (_sensitivity == sensitivity) return;

    _sensitivity = sensitivity;
    emit sensitivityChanged(_sensitivity);
}

quint8 SettingsDataModel::videoTimeout() const
{
    return _videoTimeout;
}

QStringList SettingsDataModel::presets() const
{
    QStringList presetList = _settings.childGroups();
    presetList.removeOne("UserData");
    return presetList;
}

void SettingsDataModel::setVideoTimeout(quint8 videoTimeout)
{
    if (_videoTimeout == videoTimeout) return;

    _videoTimeout = videoTimeout;
    emit videoTimeoutChanged(_videoTimeout);
}

void SettingsDataModel::setVideoTimeout(QString timeout)
{
    setVideoTimeout(timeout.toInt());
}

void SettingsDataModel::setPreset(QString preset)
{
    if (_preset == preset) return;

    _preset = preset;
    emit presetChanged(_preset);

    _settings.beginGroup("UserData");
    _settings.setValue("Preset", _preset);
    _settings.endGroup();
}

void SettingsDataModel::savePreset(QString preset)
{
    _settings.beginGroup(preset);
    bool isSystemPreset = _settings.value("IsSystemPreset", false).toBool();
    _settings.endGroup();

    if (isSystemPreset)
    {
        emit error("System preset already defined at that name, please choose a different name.");
        return;
    }

    setPreset(preset);

    saveSetting("VideoURL", _videoUrl);
    saveSetting("AudioURL", _audioUrl);
    saveSetting("UserName", _username);
    saveSetting("Password", _password);
    saveSetting("AudioPeakLevel", _sensitivity);
    saveSetting("VideoTimeout", _videoTimeout);
    _isSystemPreset = false;
}

void SettingsDataModel::removePreset(QString preset)
{
    if (isSystemPreset())
    {
        emit error("Unable to remove system presets");
        return;
    }

    _settings.beginGroup(preset);
    _settings.remove("");
    _settings.endGroup();
}

int SettingsDataModel::outputVolume() const
{
    return _outputVolume;
}

void SettingsDataModel::setOutputVolume(int arg)
{
    if (_outputVolume != arg) {
        _outputVolume = arg;
        emit outputVolumeChanged(arg);
    }
}

bool SettingsDataModel::isSystemPreset() const
{
    return _isSystemPreset;
}

void SettingsDataModel::loadPreset(QString preset)
{
    _settings.beginGroup(preset);
    _preset = preset;
    _videoUrl = _settings.value("VideoURL").toString();
    _audioUrl = _settings.value("AudioURL").toString();
    _username = _settings.value("UserName").toString();
    _password = _settings.value("Password").toString();
    _sensitivity = _settings.value("AudioPeakLevel").toInt();
    _videoTimeout = _settings.value("VideoTimeout").toInt();
    _isSystemPreset = _settings.value("IsSystemPreset", false).toBool();
    _settings.endGroup();

    emit refreshSettingsRequested();
}

void SettingsDataModel::saveSetting(QString key, QVariant value)
{
    _settings.beginGroup(_preset);
    _settings.setValue(key, value);
    _settings.endGroup();
}
