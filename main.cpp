/*
MoniBabyTor - a webcam based monitoring app for babies
    Copyright (C) 2013 Assaf Paz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QtGui/QApplication>

#include <qplatformdefs.h>
#include "cameralogic.h"
#include "mjpegfile.h"
#include "wavfile.h"

#if defined(MEEGO_VERSION_MAJOR) || defined(Q_WS_SIMULATOR)
#include <QtDeclarative>
#include "qmlapplicationviewer.h"
#include "videoview.h"
#else
#include "mainwindow.h"
#endif

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setApplicationName("MoniBabyTor");
    a.setOrganizationName("damagedspline");
    a.setApplicationVersion("0.1");

    SettingsDataModel dataModel;
    MJpegFile mjpegFile;
    WavFile waveFile;
    waveFile.onPeakThresholdChanged(dataModel.sensitivity());

    CameraLogic cl(dataModel, mjpegFile, waveFile);

#if defined(MEEGO_VERSION_MAJOR) || defined(Q_WS_SIMULATOR)
    qmlRegisterType<VideoView>("monibabytor", 1, 0, "VideoView");
    QmlApplicationViewer viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationAuto);
    viewer.rootContext()->setContextProperty("dataModel", &dataModel);
    viewer.rootContext()->setContextProperty("videoFeed", &mjpegFile);
    viewer.setMainQmlFile(QLatin1String("qml/MoniBabyTor/MainWindow.qml"));
    viewer.showExpanded();
#else
    MainWindow w(dataModel, mjpegFile);
    w.show();
#endif

    return a.exec();
}
