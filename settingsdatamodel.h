/*
MoniBabyTor - a webcam based monitoring app for babies
    Copyright (C) 2013 Assaf Paz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef SETTINGSDATAMODEL_H
#define SETTINGSDATAMODEL_H

#include <QObject>
#include <QSettings>
#include <QStringList>

class SettingsDataModel : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString preset READ preset WRITE setPreset NOTIFY presetChanged)

    Q_PROPERTY(QString videoUrl READ videoUrl WRITE setVideoUrl NOTIFY videoUrlChanged)
    Q_PROPERTY(QString audioUrl READ audioUrl WRITE setAudioUrl NOTIFY audioUrlChanged)
    Q_PROPERTY(QString username READ username WRITE setUsername NOTIFY usernameChanged)
    Q_PROPERTY(QString password READ password WRITE setPassword NOTIFY passwordChanged)
    Q_PROPERTY(int sensitivity READ sensitivity WRITE setSensitivity NOTIFY sensitivityChanged)
    Q_PROPERTY(quint8 videoTimeout READ videoTimeout WRITE setVideoTimeout NOTIFY videoTimeoutChanged)
    Q_PROPERTY(int outputVolume READ outputVolume WRITE setOutputVolume NOTIFY outputVolumeChanged)
    Q_PROPERTY(bool isSystemPreset READ isSystemPreset)

public:
    explicit SettingsDataModel(QObject *parent = 0);

    QString preset() const;
    QString videoUrl() const;
    QString audioUrl() const;
    QString username() const;
    QString password() const;
    int sensitivity() const;
    quint8 videoTimeout() const;
    int outputVolume() const;

    QStringList presets() const;

    bool isSystemPreset() const;

signals:
    void presetChanged(QString);
    void videoUrlChanged(QString);
    void audioUrlChanged(QString);
    void usernameChanged(QString);
    void passwordChanged(QString);
    void sensitivityChanged(int);
    void videoTimeoutChanged(quint8);

    void error(QString errorMessage);
    void rmsLevelChanged(int level);
    void startRequested();
    void stopRequested();
    void videoTimeoutRequested();
    void showSettingsRequested();
    void quickViewRequested();
    void refreshSettingsRequested();
    
    void outputVolumeChanged(int arg);

public slots:
    void setPreset(QString preset);
    void savePreset(QString preset);
    void removePreset(QString preset);
    void setVideoUrl(QString videoUrl);
    void setAudioUrl(QString audioUrl);
    void setUsername(QString username);
    void setPassword(QString password);
    void setSensitivity(int sensitivity);
    void setVideoTimeout(quint8 timeout);
    void setVideoTimeout(QString timeout);
    void setOutputVolume(int arg);

    void loadPreset(QString preset);

private:
    void saveSetting(QString key, QVariant value);

    QString _preset;
    QString _videoUrl;
    QString _audioUrl;
    QString _username;
    QString _password;
    int _sensitivity;
    quint8 _videoTimeout;
    QSettings _settings;
    quint8 _outputVolume;
    bool _isSystemPreset;
};

#endif // SETTINGSDATAMODEL_H
