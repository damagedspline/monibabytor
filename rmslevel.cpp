/*
MoniBabyTor - a webcam based monitoring app for babies
    Copyright (C) 2013 Assaf Paz

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "rmslevel.h"
#include <QPainter>
#include <QPen>

RmsLevel::RmsLevel(QWidget *parent) :
    QProgressBar(parent)
{
}

void RmsLevel::setThershold(int value)
{
    _threshold = value;
}

void RmsLevel::paintEvent(QPaintEvent *ev)
{
    static const int LineWidth = 3;
    QProgressBar::paintEvent(ev);

    int x = (_threshold * width() - LineWidth) / (maximum()-minimum());
    QLine line(x, 0, x, height());

    QPainter p(this);
    QPen pen(Qt::red);
    pen.setWidth(LineWidth);
    p.setPen(pen);
    p.drawLine(line);
}
