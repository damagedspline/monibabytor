#ifndef VIDEOVIEW_H
#define VIDEOVIEW_H

#include <QDeclarativeItem>
#include "videowidget.h"

class VideoView : public QDeclarativeItem
{
    Q_OBJECT

    Q_PROPERTY(QObject* videoFeed READ videoFeed WRITE setVideoFeed)

public:
    explicit VideoView(QDeclarativeItem *parent = 0);
    
    virtual ~VideoView();

    QObject* videoFeed();

signals:
    
public slots:
    void onVideoTimeout();

private slots:
    void setVideoFeed(QObject* videoFeed);
    void onImageReceived(QImage* image);
    void onGeometryChanged();

private:
    QGraphicsProxyWidget *_proxy;
    VideoWidget* _widget;
    QObject* _mjpegFile;
    bool _videoFormatInitialized;
};

#endif // VIDEOVIEW_H
