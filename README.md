# About #
* This app allows you to place a webcam with a builtin mic over your baby's crib and it will automatically start the video feed once a noise is heard.
* Originally developed for use with D-Link DCS-932L, but can work with any other camera that provides MJPEG video stream & WAV audio stream.

# License #
* GPL v3